# sshd and rsync over SSH

Uses `panubo/sshd` to provide an sshd server with rsync for receiving filesystem synchronisation from a remote lsyncd daemon.

## Usage

Add the remote users public key to `./authorized_keys`. This will be used to connect to the `USER_NAME` account configured below.

Configured using environment variables:

```shell
USER_NAME=username  # User account to host the filesystem
USER_ID=1000        # User ID
GROUP_ID=1000       # Group ID
PORT=9022           # Port for sshd daemon to listen on
```

### docker-compose.yml

The data volume shared is handled in the `volumes:` within the `docker-compose.yml` file eg.

```yaml
  volumes:
    - ${PWD}/data:/home/${USER_NAME}/data/
```
